//
//  Common.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 23/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Common typealias for JSON object
typealias JSON = [String: Any]

protocol JSONInitable {
    
    init?(with json: JSON)
    
}
