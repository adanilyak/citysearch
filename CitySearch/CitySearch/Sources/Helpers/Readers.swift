//
//  Readers.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 24/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Namespace for all readers
class Readers {
    
    /// Read cities in JSON by filename
    ///
    /// - Parameter jsonResourceName: filename
    /// - Returns: cities
    static func readCities(from jsonResourceName: String) -> [City] {
        guard let path = Bundle.init(for: self).path(forResource: jsonResourceName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [JSON] else {
                return []
        }
        
        return json.flatMap { City(with: $0) }
    }
    
}
