//
//  LexicographicCityProvider.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 23/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

// Lexicographic City Provider uses sort function for creating lexicographic order
// Sorting complexity is O(n*logn)
// Cities are stored in an array for keepeing the order correct

// Filter function is used for filtering cities by prefix
// Filtering complexity is O(n)
// The lexicographic order is saved because of sequential apply of filter

/// City provider in lexicographic order
class LexicographicCityProvider {
    
    /// Storage for cities
    fileprivate var cities: [City] = []
    
    /// Storage for filtered cities
    fileprivate var filteredCities: [City] = []
    
    /// Is filter enabled
    var isFilterEnabled: Bool = false
    
    /// Reset cities
    ///
    /// - Parameter cities: cities
    func set(with cities: [City]) {
        self.cities = cities.sorted { cityComparator(leftCity: $0, rightCity: $1) }
    }
    
    /// Comparator for lexicographic order
    /// Firstly order using name
    /// Secondly order using coutry code
    ///
    /// - Parameters:
    ///   - leftCity: first city
    ///   - rightCity: second city
    /// - Returns: is first before second
    fileprivate func cityComparator(leftCity: City, rightCity: City) -> Bool {
        if leftCity.name == rightCity.name {
            return leftCity.countryCode < rightCity.countryCode
        } else {
            return leftCity.name < rightCity.name
        }
    }
    
}

// MARK: - CityProviderProtocol
extension LexicographicCityProvider: CityProviderProtocol {
    
    func numberOfCities() -> Int {
        return isFilterEnabled ? filteredCities.count : cities.count
    }
    
    func city(for index: Int) -> City? {
        return isFilterEnabled ? filteredCities[index] : cities[index]
    }
    
}

// MARK: - Filterable
extension LexicographicCityProvider: Filterable {
    
    func apply(filter: String?, completion: ((Bool) -> Void)?) {
        guard let `filter` = filter, !filter.isEmpty else {
            let isFilterStateChanged = isFilterEnabled != false
            isFilterEnabled = false
            completion?(isFilterStateChanged)
            return
        }
        
        isFilterEnabled = true
        filteredCities = cities.filter { $0.name.lowercased().hasPrefix(filter.lowercased()) }
        completion?(true)
    }
    
}
