//
//  CityProviderProtocol.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreLocation

/// Data Source protocol for list of cities
protocol CityProviderProtocol: class {
    
    /// Total number of cities
    ///
    /// - Returns: count
    func numberOfCities() -> Int
    
    /// Get city by index
    ///
    /// - Parameter index: index
    /// - Returns: city
    func city(for index: Int) -> City?
    
}

/// Auxiliary protocol for possibility to filter something
protocol Filterable {
    
    /// Is filter enabled
    var isFilterEnabled: Bool { get set }
    
    /// Apply new fiter
    /// If nil - filter is inactive
    ///
    /// - Parameter filter: filter
    ///   - completion: callback on filter did finished applying
    func apply(filter: String?, completion: ((_ isFiltered: Bool) -> Void)?)
    
}
