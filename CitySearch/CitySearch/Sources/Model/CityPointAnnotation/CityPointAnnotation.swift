//
//  CityPointAnnotation.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 24/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import MapKit

class CityPointAnnotation: MKPointAnnotation {
    
    init(with city: City) {
        super.init()
        coordinate = city.coordinate
        title = "\(city.name), \(city.countryCode)"
    }
    
}
