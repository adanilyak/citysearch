//
//  City.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreLocation

/// Model which describes city
struct City {
    
    /// Name of the city
    let name: String
    
    /// Country code
    let countryCode: String
    
    /// Id
    let id: Int
    
    /// Coordinate (lat + lon)
    let coordinate: CLLocationCoordinate2D
    
    
}

extension City: JSONInitable {
    
    init?(with json: JSON) {
        
        guard let countryCode = json["country"] as? String,
            let name = json["name"] as? String,
            let id = json["_id"] as? Int,
            let coordinatesJSON = json["coord"] as? JSON,
            let coordinates: CLLocationCoordinate2D = CLLocationCoordinate2D(with: coordinatesJSON) else {
                return nil
        }
        
        self.name = name
        self.countryCode = countryCode
        self.id = id
        self.coordinate = coordinates
    }
    
}

extension City: Equatable {
    
    static func == (leftCity: City, rightCity: City) -> Bool {
        return leftCity.id == rightCity.id
    }
    
}

extension CLLocationCoordinate2D: JSONInitable {
    
    init?(with json: JSON) {
        guard let lon = json["lon"] as? CLLocationDegrees,
            let lat = json["lat"] as? CLLocationDegrees else {
                return nil
        }
        
        self.latitude = lat
        self.longitude = lon
    }
    
}
