//
//  MapViewController.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 24/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

/// View controller for displaying map with the city-pin
class MapViewController: UIViewController {
    
    /// Map
    @IBOutlet weak var mapView: MKMapView!
    
    /// Pin for city
    fileprivate var cityPointAnnotation: CityPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let annotation = cityPointAnnotation {
            mapView.addAnnotation(annotation)
            focusMap(on: annotation.coordinate)
            title = annotation.title
        }
        
    }
    
    /// Set new annotation
    ///
    /// - Parameter annotation: annotation
    func set(annotation: CityPointAnnotation) {
        cityPointAnnotation = annotation
    }
    
    /// Focus map on the coordinate
    /// Region is 100x100 km^2
    ///
    /// - Parameter coordinate: coordinate
    fileprivate func focusMap(on coordinate: CLLocationCoordinate2D) {
        let region: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, 100_000, 100_000)
        mapView.setRegion(region, animated: true)
    }
    
}
