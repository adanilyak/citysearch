//
//  CityListViewController.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Displays a list of cities sorted alphabetically
class CityListViewController: UITableViewController {
    
    typealias FilterableCityProvider = CityProviderProtocol & Filterable
    
    /// Data Source for list of cities
    fileprivate weak var cityProvider: FilterableCityProvider?
    
    fileprivate let searchController: UISearchController = UISearchController(searchResultsController: nil)
    
    /// Table sections
    fileprivate let sections: [Section] = [.list]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
    }
    
    func set(cityProvider: FilterableCityProvider) {
        self.cityProvider = cityProvider
        tableView.reloadData()
    }
    
    /// Setup of search results controller
    fileprivate func setupSearchController() {
        searchController.searchBar.placeholder = "City"
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        
        if #available(iOS 11, *) {
            searchController.obscuresBackgroundDuringPresentation = false
            navigationItem.searchController = searchController
        } else {
            searchController.dimsBackgroundDuringPresentation = false
            tableView.tableHeaderView = searchController.searchBar
        }
        
        definesPresentationContext = true
    }
    
}

// MARK: - Data Source
extension CityListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .list:
            return cityProvider?.numberOfCities() ?? 0
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .list:
            return dequeueReusableListSectionCell(for: indexPath)
        }
    }
    
}

// MARK: - Delegate
extension CityListViewController {
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch sections[indexPath.section] {
        case .list:
            didSelectListSectionCell(at: indexPath.row)
        }
    }
    
}

// MARK: - List Section
extension CityListViewController {
    
    fileprivate func dequeueReusableListSectionCell(for indexPath: IndexPath) -> CityPreviewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CityPreviewCell.self),
                                                       for: indexPath) as? CityPreviewCell else {
            fatalError("Cell shoud be CityPreviewCell for index path: \(indexPath)")
        }
        
        guard let city: City = cityProvider?.city(for: indexPath.row) else {
            fatalError("Can't get city for index path: \(indexPath)")
        }
        
        cell.setup(with: CityPreviewCellItem(with: city))
        
        return cell
    }
    
    fileprivate func didSelectListSectionCell(at index: Int) {
        guard let city = cityProvider?.city(for: index) else {
            return
        }
        
        performSegue(withIdentifier: SegueIdentifier.map.rawValue,
                     sender: CityPointAnnotation(with: city))
    }
    
}

// MARK: - Segues
extension CityListViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case SegueIdentifier.map.rawValue?:
            guard let cityPoint = sender as? CityPointAnnotation,
                let mapViewController = segue.destination as? MapViewController else {
                    return
            }
            
            mapViewController.set(annotation: cityPoint)
        default:
            return
        }
    }
    
}

// MARK: - UISearchResultsUpdating
extension CityListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchText: String? = searchController.searchBar.text
        
        // Using user initiated quality queue for best performance
        // Also, it helps to relive main thread for doing smooth UI things
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.cityProvider?.apply(filter: searchText,
                                     completion: { [weak self] isFiltered in
                                        // UI updates always on main thread
                                        DispatchQueue.main.async { [weak self] in
                                            if isFiltered {
                                                self?.tableView.reloadData()
                                            }
                                        }
                }
            )
        }
    }
    
}

// MARK: - Enums
extension CityListViewController {
    
    /// Section types
    ///
    /// - list: section with list of cities
    enum Section {
        case list
    }
    
    /// Segue identifiers
    ///
    /// - map: transition to map
    enum SegueIdentifier: String {
        case map
    }
    
}
