//
//  AppDelegate.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /// City provider
    fileprivate let lexicographicCityProvider: LexicographicCityProvider = LexicographicCityProvider()
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        guard let navigationController: UINavigationController = window?.rootViewController as? UINavigationController,
            let cityListViewController: CityListViewController = navigationController.viewControllers.first as? CityListViewController else {
            return true
        }
        
        // Lexicographic City Provider uses sort function for creating lexicographic order
        // Sorting complexity is O(n*logn)
        // Cities are stored in an array for keepeing the order correct
        
        // Filter function is used for filtering cities by prefix
        // Filtering complexity is O(n)
        // The lexicographic order is saved because of sequential apply of filter
        
        lexicographicCityProvider.set(with: Readers.readCities(from: "cities"))
        
        // Update city provider
        cityListViewController.set(cityProvider: lexicographicCityProvider)
        
        return true
    }

}

