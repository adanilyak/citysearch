//
//  CityPreviewCell.swift
//  CitySearch
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Item with data for CityPreviewCell
struct CityPreviewCellItem {
    
    /// Text label data
    let title: String
    
    init(with city: City) {
        self.title = "\(city.name), \(city.countryCode)"
    }
    
}

/// Cell for CityListViewController's List section
class CityPreviewCell: UITableViewCell {
    
    /// Setup cell with item
    ///
    /// - Parameter item: item
    func setup(with item: CityPreviewCellItem) {
        textLabel?.text = item.title
    }
    
}
