//
//  LexicographicCityProviderTests.swift
//  CitySearchTests
//
//  Created by Alexander Danilyak on 22/01/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import XCTest
@testable import CitySearch

class LexicographicCityProviderTests: XCTestCase {
    
    /// Cities provider
    lazy var lexicographicCityProvider: LexicographicCityProvider = {
        let provider = LexicographicCityProvider()
        provider.set(with: cities)
        return provider
    }()
    
    /// Cities from JSON file
    let cities: [City] = Readers.readCities(from: "test_cities")
    
    override func setUp() {
        super.setUp()
        
        // Restores default state
        lexicographicCityProvider.apply(filter: nil, completion: nil)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testEmptyFilter() {
        let expectedNumber: Int = lexicographicCityProvider.numberOfCities()
        
        lexicographicCityProvider.apply(filter: "") { [unowned self] isFiltered in
            let actualNumber = self.lexicographicCityProvider.numberOfCities()
            XCTAssert(expectedNumber == actualNumber)
        }
    }
    
    func testNilFilter() {
        let expectedNumber: Int = lexicographicCityProvider.numberOfCities()
        
        lexicographicCityProvider.apply(filter: nil) { [unowned self] isFiltered in
            let actualNumber = self.lexicographicCityProvider.numberOfCities()
            XCTAssert(expectedNumber == actualNumber)
        }
    }
    
    func testSpecialCharactersFilter() {
        let expectedNumber: Int = 0
        
        lexicographicCityProvider.apply(filter: "(*!@") { [unowned self] isFiltered in
            let actualNumber = self.lexicographicCityProvider.numberOfCities()
            XCTAssert(expectedNumber == actualNumber)
        }
    }
    
    func testNormalFilter() {
        let expectedNumber: Int = 2
        
        lexicographicCityProvider.apply(filter: "M") { [unowned self] isFiltered in
            let actualNumber = self.lexicographicCityProvider.numberOfCities()
            XCTAssert(expectedNumber == actualNumber)
        }
    }
    
}
